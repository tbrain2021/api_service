from argparse import ArgumentParser
import io
import os
import base64
import datetime
import hashlib
from PIL import Image

import cv2
from flask import Flask
from flask import request
from flask import jsonify
import numpy as np

import tensorflow as tf
from tensorflow.keras.models import load_model

### u2net ####
from u2net import u2net_model

os.environ["CUDA_VISIBLE_DEVICES"] = "1"

classes = open('classes.txt', 'r', encoding='utf-8').read().split('\n')
model = load_model('effB0_0616.h5')

app = Flask(__name__)

####### PUT YOUR INFORMATION HERE #######
CAPTAIN_EMAIL = 'birth0106@gmail.com '  #
SALT = '005007'                         #
#########################################


def generate_server_uuid(input_string):
    """ Create your own server_uuid.

    @param:
        input_string (str): information to be encoded as server_uuid
    @returns:
        server_uuid (str): your unique server_uuid
    """
    s = hashlib.sha256()
    data = (input_string + SALT).encode("utf-8")
    s.update(data)
    server_uuid = s.hexdigest()
    return server_uuid


def base64_to_binary_for_cv2(image_64_encoded):
    """ Convert base64 to numpy.ndarray for cv2.

    @param:
        image_64_encode(str): image that encoded in base64 string format.
    @returns:
        image(numpy.ndarray): an image.
    """
    img_base64_binary = image_64_encoded.encode("utf-8")
    img_binary = base64.b64decode(img_base64_binary)
    image = cv2.imdecode(np.frombuffer(img_binary, np.uint8), cv2.IMREAD_COLOR)
    return image


def bbox_by_black(img):
    block = 2
    center_distance_ratio = 1 / 4

    top = 0
    bot = 0
    left = 0
    right = 0

    for y in range(int(img[:, :, 0].shape[0] / 2 + img[:, :, 0].shape[0] * center_distance_ratio),
                   img[:, :, 0].shape[0] - 1):
        if img[y, :, 0].mean() < block:
            top = y
            break
    if top == 0:
        top = img[:, :, 0].shape[0]

    for y in range(int(img[:, :, 0].shape[0] / 2 - img[:, :, 0].shape[0] * center_distance_ratio), 0, -1):
        if img[y, :, 0].mean() < block:
            bot = y
            break

    for x in range(int(img[bot:top, :, 0].shape[1] / 2 + img[bot:top, :, 0].shape[1] * center_distance_ratio),
                   img[bot:top, :, 0].shape[1] - 1):
        if img[bot:top, x, 0].mean() < block:
            right = x
            break

    if right == 0:
        right = img[bot:top, :, 0].shape[1]

    for x in range(int(img[bot:top, :, 0].shape[1] / 2 - img[bot:top, :, 0].shape[1] * center_distance_ratio), 0, -1):
        if img[bot:top, x, 0].mean() < block:
            left = x
            break

    return left, right, bot, top

def preprocess(file_path, img_size=224):
    img_bgr = cv2.imread(file_path)
    thresh = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2RGB)
    x1, x2, y1, y2 = bbox_by_black(thresh)
    crop_img = img_bgr[y1:y2, x1:x2].astype('float32')
    img = tf.image.resize_with_pad(crop_img, img_size, img_size)
    return img

def predict(image, uuid):
    """ Predict your model result.

    @param:
        image (numpy.ndarray): an image.
    @returns:
        prediction (str): a word.
    """

    ####### PUT YOUR MODEL INFERENCING CODE HERE #######
    prediction = 'isnull'
    img_size = 224
    image = cv2.cvtColor(np.asarray(image), cv2.COLOR_RGB2BGR)
    thresh = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    x1, x2, y1, y2 = bbox_by_black(thresh)
    crop_img = image[y1:y2, x1:x2]
    img = tf.image.resize_with_pad(crop_img, img_size, img_size)

    inputs = np.expand_dims(img, axis=0)

    pred = model.predict(inputs)
    prediction = classes[pred.argmax(-1)[0]]
    pred_conf = pred[0][pred.argmax(-1)[0]]
    print(uuid, prediction, pred_conf)
    if pred_conf <= 0.5:
        prediction = 'isnull'

    ####################################################
    if _check_datatype_to_string(prediction):
        return prediction


def _check_datatype_to_string(prediction):
    """ Check if your prediction is in str type or not.
        If not, then raise error.

    @param:
        prediction: your prediction
    @returns:
        True or raise TypeError.
    """
    if isinstance(prediction, str):
        return True
    raise TypeError('Prediction is not in string type.')


@app.route('/inference', methods=['POST'])
def inference():
    """ API that return your model predictions when E.SUN calls this API. """
    start_time = datetime.datetime.now()
    data = request.get_json(force=True)

    # 自行取用，可紀錄玉山呼叫的 timestamp
    esun_timestamp = data['esun_timestamp']

    # 取 image(base64 encoded) 並轉成 cv2 可用格式
    image_64_encoded = data['image']

    # u2net
    image = u2net_model(image_64_encoded)
    image.save("upload/{}_u2net.jpg".format(data['esun_uuid'][0:5]))
    # image2 = base64_to_binary_for_cv2(image_64_encoded)

    t = datetime.datetime.now()
    ts = str(int(t.utcnow().timestamp()))
    server_uuid = generate_server_uuid(CAPTAIN_EMAIL + ts)

    try:
        answer = predict(image, data['esun_uuid'])
    except TypeError as type_error:
        # You can write some log...
        raise type_error
    except Exception as e:
        # You can write some log...
        raise e

    imgdata = base64.b64decode(str(image_64_encoded))
    image2 = Image.open(io.BytesIO(imgdata))
    image2.save('upload/{}_{}.jpg'.format(data['esun_uuid'][0:5],answer))

    #server_timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    now = datetime.datetime.now()
    server_timestamp = int(datetime.datetime.timestamp(now))
    end_time = datetime.datetime.now() - start_time

    return jsonify({'esun_uuid': data['esun_uuid'],
                    'server_uuid': server_uuid,
                    'answer': answer,
                    #'runtime': str(end_time),
                    'server_timestamp': server_timestamp})


if __name__ == "__main__":
    arg_parser = ArgumentParser(
        usage='Usage: python ' + __file__ + ' [--port <port>] [--help]'
    )
    arg_parser.add_argument('-p', '--port', default=8080, help='port')
    arg_parser.add_argument('-d', '--debug', default=True, help='debug')
    options = arg_parser.parse_args()

    app.run(host="0.0.0.0", debug=options.debug, port=options.port)
