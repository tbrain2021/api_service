import base64
import torch
from torchvision import transforms
from torch.utils.data import DataLoader
from torch.autograd import Variable
from skimage import io, transform, color
import numpy as np
from PIL import Image
import cv2

from model import U2NET # full size version 173.6 MB
from model import U2NETP # small version u2net 4.7 MB

import os
os.environ["CUDA_VISIBLE_DEVICES"] = "1"

# model_name = 'u2net'#u2netp
# model_dir = 'u2net_seg_word.pth'
model_name = 'u2netp'
model_dir = 'u2netp_seg_word.pth'

if (model_name == 'u2net'):
    print("...load U2NET---173.6 MB")
    net = U2NET(3, 1)
elif (model_name == 'u2netp'):
    print("...load U2NEP---4.7 MB")
    net = U2NETP(3, 1)

if torch.cuda.is_available():
    net.load_state_dict(torch.load(model_dir))
    net.cuda()
else:
    net.load_state_dict(torch.load(model_dir, map_location='cpu'))

net.eval()

def u2net_model(image_64_encoded):
    img_base64_binary = image_64_encoded.encode("utf-8")
    img_binary = base64.b64decode(img_base64_binary)

    image = io.imread(img_binary, plugin='imageio')
    w, h, _ = image.shape
    image = cv2.resize(image, (288, 288))
    tmpImg = np.zeros((image.shape[0], image.shape[1], 3))
    image = image / np.max(image)

    tmpImg[:, :, 0] = (image[:, :, 0] - 0.485) / 0.229
    tmpImg[:, :, 1] = (image[:, :, 1] - 0.456) / 0.224
    tmpImg[:, :, 2] = (image[:, :, 2] - 0.406) / 0.225

    tmpImg = tmpImg.transpose((2, 0, 1))
    inputs_test = torch.from_numpy(tmpImg)
    inputs_test = inputs_test.type(torch.FloatTensor)

    if torch.cuda.is_available():
        inputs_test = Variable(inputs_test.cuda())
    else:
        inputs_test = Variable(inputs_test)

    inputs_test = inputs_test.unsqueeze(0)

    d1, d2, d3, d4, d5, d6, d7 = net(inputs_test)

    pred = d1[:, 0, :, :]
    ma = torch.max(pred)
    mi = torch.min(pred)

    predict = (pred - mi) / (ma - mi)
    predict = predict.squeeze()
    predict_np = predict.cpu().data.numpy()
    predict_np = cv2.resize(predict_np, (h, w))

    im = Image.fromarray(predict_np * 255).convert('RGB')

    return im